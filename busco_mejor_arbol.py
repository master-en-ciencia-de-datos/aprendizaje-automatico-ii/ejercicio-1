"""
Este script se utiliza para encontrar los mejores parámetros de un árbol de
decisión para el dataset de Spam usando todas las variables (m=56) y raiz de 56.
"""

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
import numpy as np

RANDOM_SEED = 42
TRAIN_TEST_SPLIT = 0.2

# Importamos el dataset de spam:

df = pd.read_csv("spam.data", sep=" ", header=None,
                 names=list(range(57)) + ['spam'])

# El vector X se reune en las columnas 0 a la 56 y el label es la columna 'spam'
# Hay que aleatorizar los índices.

df = df.sample(frac=1, random_state=RANDOM_SEED).reset_index(drop=True)
y = df.pop('spam')
X = df

X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=TRAIN_TEST_SPLIT,
                                                    random_state=RANDOM_SEED)

# clf = RandomForestClassifier(max_features=56, oob_score=False, n_jobs=3)
# clf.fit(X_train, y_train)

# print("Con m=raiz de p")
# param_grid = {'n_estimators': [50, 75, 100, 125, 150, 175, 200, 225, 250],
#               'min_samples_split': np.arange(2, 10),
#               'max_depth': np.arange(1, 21)}
#
# tree = GridSearchCV(RandomForestClassifier(max_features='auto'),
#                     param_grid,
#                     cv=5,
#                     verbose=1,
#                     n_jobs=3)
#
# tree.fit(X_train, y_train)
# print(tree.best_params_)
#
# predictions = tree.predict(X_test)
# print(accuracy_score(predictions, y_test))

print("Con m=56")
param_grid = {'n_estimators': [50, 75, 100, 125, 150, 175, 200, 225, 250],
              'min_samples_split': np.arange(2, 10),
              'max_depth': np.arange(1, 21)}

tree = GridSearchCV(RandomForestClassifier(max_features=None),
                    param_grid,
                    cv=5,
                    verbose=1,
                    n_jobs=4)

tree.fit(X_train, y_train)
print(tree.best_params_)
predictions = tree.predict(X_test)
print(accuracy_score(predictions, y_test))
