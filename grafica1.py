"""
Este script se utiliza para representar el comportamiento del error
OOB, "out-of-bag", y del error del test de un RandomForest en función
de m, "max_features".
"""

from utils import train_test_forest, lista_de_listas_a_lista_y_error
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np

ITERACIONES = 20  # Iteraciones por cada m
LIST_OF_M = list(range(1, 57))  # [1, 2, 3, ..., 56]

"""
list_of_lists_of_oob_error  = []
list_of_lists_of_test_error = []
# lista_de_listas_a_lista_y_error(LISTA)
for i in range(ITERACIONES):
    print(f"Iteración {i+1}:")
    list_of_oob_error  = []
    list_of_test_error = []
    for m in tqdm(LIST_OF_M, desc='valor m'):
        oob_score, test_score = train_test_forest(m)
        list_of_oob_error.append(1 - oob_score)
        list_of_test_error.append(1 - test_score)

    list_of_lists_of_oob_error.append(list_of_oob_error)
    list_of_lists_of_test_error.append(list_of_test_error)

oob_error_mean, oob_error_std  = lista_de_listas_a_lista_y_error(list_of_lists_of_oob_error)
test_error_mean, test_error_std = lista_de_listas_a_lista_y_error(list_of_lists_of_test_error)

np.save("medidas/oob_error_mean.npy", oob_error_mean)
np.save("medidas/oob_error_std.npy", oob_error_std)
np.save("medidas/test_error_mean.npy", test_error_mean)
np.save("medidas/test_error_std.npy", test_error_std)
"""

oob_error_mean = np.load("medidas/oob_error_mean.npy")
oob_error_std = np.load("medidas/oob_error_std.npy")
test_error_mean = np.load("medidas/test_error_mean.npy")
test_error_std = np.load("medidas/test_error_std.npy")


plt.rcParams["figure.figsize"] = (20, 12)

plt.plot(LIST_OF_M, oob_error_mean, label="error oob", color="royalblue")
plt.fill_between(LIST_OF_M, oob_error_mean - oob_error_std,
                 oob_error_mean + oob_error_std,
                 alpha=0.2, edgecolor='lightsteelblue',
                 facecolor='cornflowerblue',
                 linewidth=4, linestyle='dashdot', antialiased=True)

plt.plot(LIST_OF_M, test_error_mean, label="error test", color="orange")
plt.fill_between(LIST_OF_M, test_error_mean - test_error_std,
                 test_error_mean + test_error_std,
                 alpha=0.2, edgecolor='sandybrown',
                 facecolor='lightsalmon',
                 linewidth=4, linestyle='dashdot', antialiased=True)

plt.hlines(np.min(test_error_mean), -1, 57, linestyles="dashed", colors="darkorange")
plt.vlines(LIST_OF_M[-1]**0.5, 0.046, 0.0687, linestyles="dashed", colors="black")
plt.text(LIST_OF_M[-1]**0.5, np.max(test_error_std+test_error_mean) - 0.0005,
         r"  $\sqrt{p}$", fontdict={'size': 18})
plt.text(50.5, np.min(test_error_mean) - 0.0005, "min test error",
         fontdict={'color': 'darkorange', 'size': 18})
plt.xticks(np.arange(LIST_OF_M[0]-1, LIST_OF_M[-1], 5.0))
plt.xlabel("m\n Número máximo de variables usadas por el árbol",
           style='italic', fontsize=20)
plt.ylabel("Error", fontsize=20)
plt.legend(fontsize=18)
plt.grid()
plt.suptitle("Precisión de un clasificador RandomForest en función del parámetro m",
             fontsize= 28)
plt.xlim([-1, LIST_OF_M[-1]+1])
plt.ylim([0.047, 0.063])
plt.tick_params(labelsize=15)
plt.tight_layout(7.5)
plt.savefig("Figure_1.png")
plt.show()