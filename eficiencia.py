"""
Este script se utiliza para representar el comportamiento del
coste computacional en función de m.
"""

"""
Este script se utiliza para representar el comportamiento del error
OOB, "out-of-bag", y del error del test de un RandomForest en función
de m, "max_features".
"""

from utils import train_test_forest, lista_de_listas_a_lista_y_error
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np

SPLITS_SEEDS = [1, 2, 3, 4, 5]
ITERACIONES = 10  # Iteraciones por cada m
LIST_OF_M = list(range(1, 57))  # [1, 2, 3, ..., 56]

RUN_CALC = False

fig, ax1 = plt.subplots(figsize=(20, 12))

if RUN_CALC:

    seed_list_of_lists_of_oob_error  = []
    seed_list_of_lists_of_test_error = []
    seed_list_of_lists_of_time_train = []
    seed_list_of_lists_of_time_test  = []

    for split_seed in SPLITS_SEEDS:
        print(f"Semilla {split_seed}:")
        list_of_lists_of_oob_error  = []
        list_of_lists_of_test_error = []
        list_of_lists_of_time_train = []
        list_of_lists_of_time_test  = []
        for i in range(ITERACIONES):
            print(f"   Iteración {i+1}:")
            list_of_oob_error  = []
            list_of_test_error = []
            list_of_time_train = []
            list_of_time_test = []
            for m in tqdm(LIST_OF_M, desc='valor m'):
                oob_score, test_score, time_train, time_test = train_test_forest(m, give_time=True, split_seed=split_seed)
                list_of_time_train.append(time_train)
                list_of_time_test.append(time_test)
                list_of_oob_error.append(1 - oob_score)
                list_of_test_error.append(1 - test_score)

            list_of_lists_of_oob_error.append(list_of_oob_error)
            list_of_lists_of_test_error.append(list_of_test_error)
            list_of_lists_of_time_train.append(list_of_time_train)
            list_of_lists_of_time_test.append(list_of_time_test)

        oob_error_mean, oob_error_std  = lista_de_listas_a_lista_y_error(list_of_lists_of_oob_error)
        test_error_mean, test_error_std = lista_de_listas_a_lista_y_error(list_of_lists_of_test_error)
        time_train_mean, time_train_std = lista_de_listas_a_lista_y_error(list_of_lists_of_time_train)
        time_test_mean, time_test_std = lista_de_listas_a_lista_y_error(list_of_lists_of_time_test)

        seed_list_of_lists_of_oob_error.append([oob_error_mean, oob_error_std])
        seed_list_of_lists_of_test_error.append([test_error_mean, test_error_std])
        seed_list_of_lists_of_time_train.append([time_train_mean, time_train_std])
        seed_list_of_lists_of_time_test.append([time_test_mean, time_test_std])

    np.save("medidas/seed_list_of_lists_of_oob_error.npy", seed_list_of_lists_of_oob_error)
    np.save("medidas/seed_list_of_lists_of_test_error.npy", seed_list_of_lists_of_test_error)
    np.save("medidas/seed_list_of_lists_of_time_train.npy", seed_list_of_lists_of_time_train)
    np.save("medidas/seed_list_of_lists_of_time_test.npy", seed_list_of_lists_of_time_test)

else:
    seed_list_of_lists_of_oob_error = np.load("medidas/seed_list_of_lists_of_oob_error.npy")
    seed_list_of_lists_of_test_error = np.load("medidas/seed_list_of_lists_of_test_error.npy")
    seed_list_of_lists_of_time_train = np.load("medidas/seed_list_of_lists_of_time_train.npy")
    seed_list_of_lists_of_time_test = np.load("medidas/seed_list_of_lists_of_time_test.npy")

# Se quiere representar los tests por separado y el oob promedio

oob_error_mean, oob_error_std = np.mean(seed_list_of_lists_of_oob_error, axis=0)

ax1.plot(LIST_OF_M, oob_error_mean, label="error oob", color="royalblue")
ax1.fill_between(LIST_OF_M, oob_error_mean - oob_error_std,
                 oob_error_mean + oob_error_std,
                 alpha=0.2, edgecolor='lightsteelblue',
                 facecolor='cornflowerblue',
                 linewidth=4, linestyle='dashdot', antialiased=True)

for i, [test_error_mean, test_error_std] in enumerate(seed_list_of_lists_of_test_error):
    if i == 0:
        label = "error test"
    else:
        label = None
    ax1.plot(LIST_OF_M, test_error_mean, label=label, color="orange")
    ax1.fill_between(LIST_OF_M, test_error_mean - test_error_std,
                     test_error_mean + test_error_std,
                     alpha=0.2, edgecolor='sandybrown',
                     facecolor='lightsalmon',
                     linewidth=4, linestyle='dashdot', antialiased=True)

# ax1.hlines(np.min(test_error_mean), -1, 57, linestyles="dashed", colors="darkorange")
ax1.vlines(LIST_OF_M[-1]**0.5, 0.0, 0.084, linestyles="dashed", colors="black")
ax1.text(LIST_OF_M[-1]**0.5, 0.08,
         r"  $\sqrt{p}$", fontdict={'size': 18})


ax1.set_xticks(np.arange(LIST_OF_M[0]-1, LIST_OF_M[-1], 5.0))
ax1.set_xlabel("m\n Número máximo de variables usadas por el árbol",
           style='italic', fontsize=20)
ax1.set_ylabel("Error", fontsize=20)
ax1.legend(bbox_to_anchor=(0.9, 0.4), fontsize=18)
ax1.grid()
plt.suptitle("Precisión de un clasificador RandomForest en función del parámetro m\npara distintas divisiones del dataset",
             fontsize= 28)
ax1.set_xlim([-1, LIST_OF_M[-1]+1])
ax1.set_ylim([0.0, 0.084])
ax1.tick_params(labelsize=15)

ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis


color = 'tab:red'
ax2.set_ylabel('Tiempo de cómputo (s)', fontsize=20)
time_train_mean, time_train_std = np.mean(seed_list_of_lists_of_time_train, axis=0)
ax2.errorbar(LIST_OF_M, time_train_mean, yerr=time_train_std, linestyle="-.",
             color=color, elinewidth=0.5, label="Tiempo de entrenamiento")

color = 'tab:green'
time_test_mean, time_test_std = np.mean(seed_list_of_lists_of_time_test, axis=0)
ax2.errorbar(LIST_OF_M, time_test_mean, yerr=time_test_std, linestyle="-.",
             color=color, label="Tiempo de test")

ax2.tick_params(axis='y', labelsize=15)
ax2.set_ylim([0, 1.6])
ax2.legend(bbox_to_anchor=(1.0, 0.2), fontsize=18)
#fig.tight_layout(7.5)

fig.savefig("Figure_1.png")


# plt.show()