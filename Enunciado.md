# Ejercicio 2 (fecha entrega 13/04)

## Formato
Un fichero PDF con dos caras de folio, con lo siguiente:

1. La primera cara de folio contendrá, en su mitad superior, **una única gráfica**, 
que ocupará todo el espacio de esa mitad superior. Los textos de la gráfica 
(ejes, etiquetas, leyenda...) tendrán un tamaño de fuente equivalente al del 
cuerpo del texto (~12pt).

2. En el resto de la primera y en la segunda cara, habrá unos párrafos (con 
fuente a 12pt y con márgenes de 2.5 cm) analizando la gráfica y valorando de 
forma crítica los resultados. Si habéis hecho más de un experimento, contadlo y 
contad vuestras conclusiones, pero elegid una única gráfica para entregarla.

Tened en cuenta que este PDF será leído mostrando ambas caras simultáneamente en
una pantalla de dimensiones 16:9.

## Evaluación

- Gráfica (se valorará que su realización sea adecuada al estándar científico): 
3 puntos
- Análisis y valoración: 6 puntos
    - Análisis de la relación entre la curva OOB y la curva de error de test: 2 
puntos
    - Análisis de la influencia práctica del parámetro m: 2 puntos
    - Consideraciones y pruebas sobre otros parámetros: 2 puntos
- Pregunta: ¿cómo afecta el parámetro m a la eficiencia computacional del 
algoritmo? ¿Por qué?: 1 punto

Sed sintéticos e ir al grano.

## Comentarios

Si vemos la primera parte del ejercicio como la tarea de crear la gráfica, ya
lo he hecho en el ejercicio 1. Sin embargo, podemos intentar hacer algo de CV
para que sea un estudio más completo.

Recapitulamos **lo que se hizo** en el primer ejercicio:

1. Se extraen los datos del csv de spam a los conjuntos X e y.
2. Se crea una lista de valores de m desde 1 hasta 56, el total de variables.
3. Se entrenan varios árboles para cada m y se representa la media y las 
desviaciones como errores.

El resultado fue que eran similares.

Habrá que hacer estas gráficas con otros parámetros del árbol a ver si el 
restultado es **consistente** o no. Con esto se respondería a lo de 
```Consideraciones y pruebas sobre otros parámetros```, más o menos.

Para la pregunta de 
```¿cómo afecta el parámetro m a la eficiencia computacional del algoritmo? ¿Por qué?:```
habrá que calcular el tiempo de entrenamiento y predicción para distintos valores
de m. También estaría bien probar otros parámetros.

Como todo esto no va a entrar en una gráfica, lo mejor será incluir tablas.

### Creación de la gráfica

Eje Y: Error (científico?) // Eje X: m // Añadir grid, error, título, leyenda. // Ver ylim 
para que quede claro. Pie de figura!!!

Primero habría que encontrar el árbol que mejor resultados dé para todas las m.
Esta decisión se hace para no influir en la gráfica, ya que si buscamos los 
mejores parámetros para un m dado en la gráfica de los errores va a sobresalir
el correspondiente a ese valor.

Aunque por defecto se suelen coger raiz de m... Hago la prueba a ver si los 
parámetros que salen son muy distintos.

Jugamos con
'n_estimators': [50, 75, 100, 125, 150, 175, 200, 225, 250],
'min_samples_split': np.arange(2, 10),
'max_depth': np.arange(1, 21)}

Para raiz de m: 
{'max_depth': 19, 'min_samples_split': 2, 'n_estimators': 75}
0.9587404994571118

Para m: 

{'max_depth': 19, 'min_samples_split': 3, 'n_estimators': 125}
0.9478827361563518

Conclusión: buscando el mejor árbol de entre los parámetros escogidos, 
se obtiene mejor resultado para raiz de m que para m. Ahora con estos 
parámetros se hace la gráfica de errpr vs m. Cambiamos la semilla del
train test split para que no influya el haber escogido estos parámetros.

### Discusión de resultados

Comenzar comentando lo que se quiere:

    Ex. 15.6 Fit a series of random-forest classifiers to the spam data, to 
    explore the sensitivity to the parameter m. Plot both the oob error as well 
    as the test error against a suitably chosen range of values for m.