"""
Funciones de ayuda para grafica1.py
"""

import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import numpy as np
import time

params = {'max_depth': 19, 'min_samples_split': 2, 'n_estimators': 75}
RANDOM_SEED = 24
TRAIN_TEST_SPLIT = 0.2
df = pd.read_csv("spam.data", sep=" ", header=None,
                 names=list(range(57)) + ['spam'])
df = df.sample(frac=1, random_state=RANDOM_SEED).reset_index(drop=True)
y = df.pop('spam')
X = df

X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                    test_size=TRAIN_TEST_SPLIT,
                                                    random_state=RANDOM_SEED)


def train_test_forest(m, give_time=False, split_seed=None):
    """
    Entrena un RandomForest con los parámetros predeterminados de Sklearn
    y m 'max_features'.

    :param split_seed: semilla aleatoria de la partición en train y test.
    :param give_time: devuelve los tiempos además de los errores.
    :param m: máximas variables a utilizar en el entrenamiento del árbol.
    :return: OOB score, test score
    """
    global X_train, X_test, y_train, y_test
    if split_seed is not None and type(split_seed) is int:
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                            test_size=TRAIN_TEST_SPLIT,
                                                            random_state=split_seed)

    clf = RandomForestClassifier(max_features=m, oob_score=True, n_jobs=4,
                                 **params)
    start_time = time.time()
    clf.fit(X_train, y_train)
    time_train = time.time() - start_time
    start_time = time.time()
    y_pred = clf.predict(X_test)
    test_score = accuracy_score(y_test, y_pred,
                                normalize=True,
                                sample_weight=None)
    time_test = time.time() - start_time
    if not give_time:
        return clf.oob_score_, test_score
    else:
        return clf.oob_score_, test_score, time_train, time_test


def lista_de_listas_a_lista_y_error(lista):
    """
    Convierte un lista de arrays de numpy en una
    :param lista:
    :return:
    """
    mean = np.mean(lista, axis=0)
    std  = np.std(lista, axis=0)
    return mean, std